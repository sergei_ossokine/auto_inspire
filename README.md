# README #


This is a small script to automatically query inspire database
and print the formatted bibtex to screen or insert it into an
existing .bib bibliography in alphabetical order. Also copies the key
to the clipboard for easy copy-pasting back into latex.

![](auto_inspire/images/usage.gif)

### What is this repository for? ###

* Ditto
* Very alpha - 0.0.1

### How do I get set up? ###

All the hard work is handled by the following great libraries


   1. pybtex
   2. pyinspire
   3. clint 
   4. pyperclip
   
You should be able to get all prerequisites with 

`pip install pybtex pyinspire clint pyperclip`

NOTE: it appears that the version of pyinspire on pip has a small bug.
You have 2 choices currently (May 2017):

* Install from bitbucket: <https://bitbucket.org/ihuston/pyinspire>
* Change line 52 of pyinspire.py to read `url = APIURL + urlencode(inspireoptions)` 

You can also do the following

```
git clone git@bitbucket.org:sergei_ossokine/auto_inspire.git
cd auto_inspire/
pip install -r requirements.txt
python setup.py install
cd ~/
auto_inspire.py -h
```

### Contribution guidelines ###

* Please contibute!

### Who do I talk to? ###

* Serguei
