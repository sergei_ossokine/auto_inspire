from setuptools import setup

setup(name='auto_inspire',
      version='0.0.1',
      description='Script to automatically query inspire and insert results into bib files',
      url='',
      author='Serguei Ossokine',
      author_email='',
      license='MIT',
      packages=['auto_inspire'],
      scripts=['auto_inspire/auto_inspire.py'],
      setup_requires=['bs4'],
      install_requires=[
          'bs4',
          'pyperclip',
          'clint',
          'pybtex',
          'pyinspire',
          'lxml'
      ],
      dependency_links=["git+https://bitbucket.org/ihuston/pyinspire.git@master#egg=pyinspire-0"],
     
      zip_safe=False)
