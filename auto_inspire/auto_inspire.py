#!/usr/bin/env python
'''Small script to query inspire database, get results and
update a bib file with the resutls inserting the query in
alphabetical order.
Use this script at your own risk: it may melt your computer.
Pre-requisites:
1. pybtex
2. clint
3. pyinspire
4. pyperclip


Copyright (c) 2017 Serguei Ossokine

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
from __future__ import print_function
import argparse
import sys
import os
import re
import readline
import shutil
import clint
import pybtex.database
import pyperclip
from pybtex.database import BibliographyData, BibliographyDataError
from pybtex.exceptions import PybtexError


import pyinspire as pyins
from clint.textui import puts, indent,colored, prompt





def get_comments(fname):
    '''Extract comments from file to save them later'''
    cmts = ''
    with open(fname, 'r') as fp:
        for line in fp:
            if line[0] == '%':
                cmts += line
    return cmts

def get_bibtex_string_for_entry(db, key):
    '''Get the proper bibtex formatting for the entry. Have
    to hack around the fact that to string is not a method of the entry
    class'''
    temp_db = BibliographyData()
    temp_db.add_entry(key, db.entries.get(key))
    return temp_db.to_string('bibtex')

def display_search_results(db):
    '''Loop over a BibliographyData object and print all the entries
    in a nice format to the terminal'''

    puts(colored.green("The following entries were found"))
    keys = db.entries.keys()
    for i in range(len(keys)):
        entr = db.entries[keys[i]]
        title = entr.fields['title'].strip('{}').encode('utf8')
        author = entr.persons['author'][0].last_names[0].encode('utf8')
        year = entr.fields['year']
        if 'doi' in entr.fields.keys():
            url = "http://dx.doi.org/"+entr.fields['doi'].split(',')[0]
        elif 'eprint' in entr.fields.keys():
            url = "https://arxiv.org/abs/"+entr.fields['eprint']
        else:
            url = ""
        tprint = "{}. {} by {}+, {} {}".format(i, title, author, year, url)
        puts(tprint)

if __name__ == '__main__':


    histfile = os.path.join(os.path.expanduser("~"), ".inspire_hist")
    if os.path.isfile(histfile):
          readline.read_history_file(histfile)

    p = argparse.ArgumentParser()
    p.add_argument('-b', '--bibfile', type=str,default=None,
                   help="The name of the bib file to modify")
    args = p.parse_args()
    puts("Welcome to auto-inspire!")
    puts("Please enter your query below. Pressing the 'up' arrow key will give \
you history, if available")
    search_string = raw_input('Search string:')
    readline.write_history_file(histfile)
    res_string = pyins.get_text_from_inspire(search=search_string,
                                       resultformat="bibtex")
    if res_string:
        new_entry = pybtex.database.parse_string(res_string, 'bibtex')
    else:
        puts(colored.red("Did not find any results for your query, exiting!"))
        sys.exit(0)
    display_search_results(new_entry)
    keys = new_entry.entries.keys()
    while 1:
        try:
            nmb = int(prompt.query('Select result:', default=None))
        except ValueError:
            puts(colored.red("Please enter a number or -1 to quit "))
            continue
        if nmb < len(keys) or nmb < 0:
            break
        else:
            puts(colored.red("Choice {} is not valid, please choose again,\
                             or type -1 to quit".format(nmb)))

    if nmb < 0:
        puts("Exiting, good-bye!")
        sys.exit(0)

    puts("You have selected {}".format(nmb))
    selected_key = keys[nmb]
    result = get_bibtex_string_for_entry(new_entry, selected_key)
    print(result)
    if args.bibfile is not None:

        puts("Would you like to insert this result into {}".format(args.bibfile))
        ans = prompt.query('Enter y/n', default='n')
        if ans == 'y':
            puts(colored.green("Parsing the bibtex database"))
            try:
                full_db = pybtex.database.parse_file(args.bibfile)
            except PybtexError:
                puts(colored.red("Could not find the bibtex file {}. Are you sure \
it exists?".format(args.bibfile)))
                sys.exit(1)

            comments = get_comments(args.bibfile)

            try:
                full_db.add_entry(selected_key, new_entry.entries[selected_key])
            except BibliographyDataError:
                puts(colored.red("The key {} already exists in the database. \
                Quitting!".format(selected_key)))
                sys.exit(1)

            puts(colored.green("Writing new entry"))
            full_db.entries.order = sorted(full_db.entries.order)
            backname = args.bibfile.split('.')[0] + "_bak.bib"
            puts(colored.yellow("Creating a back-up in {}".format(backname)))
            shutil.move(args.bibfile, backname)
            puts(colored.green("Writing bibtex file"))
            contents = full_db.to_string('bibtex')
            contents=contents.replace("\\%",'%')
            with open(args.bibfile, 'w') as fw:
                fw.write(comments)
                fw.write(contents.encode('utf8'))
    puts("The key {} has been added to the clipboard!".format(selected_key))
    pyperclip.copy(selected_key)
    puts("All done, good-bye!")

    

   
